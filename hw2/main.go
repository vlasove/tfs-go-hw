package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"time"
)

type News struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	Provider    string    `json:"provider"`
	PublushTime time.Time `json:"published_at"`
	Ticker      []string  `json:"tickers"`
}

type Feed struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"payload"`
}

type CompanyNews struct {
	PayloadCompany []News    `json:"items"`
	PublishTime    time.Time `json:"published_at"`
	Ticker         []string  `json:"tickers"`
}

func readNews(path string) ([]News, error) {
	jsonFile, err := os.Open(path)

	if err != nil {
		return nil, fmt.Errorf("can't open file: %s \nmessage : %s", path, err)
	}
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, fmt.Errorf("can't read (.ReadAll) from json : %s \nmessage : %s", path, err)
	}

	var news []News
	err = json.Unmarshal(byteValue, &news)
	if err != nil {
		return nil, fmt.Errorf("can't unmarshal file : %s", err)
	}

	return news, err

}

func newsSort(news []News) {
	sort.SliceStable(news, func(i, j int) bool {
		return news[i].PublushTime.Before(news[j].PublushTime)
	})

}

func getNewsByIndexes(indexes []int, news []News) []News {
	var newsByIndexes []News
	for _, val := range indexes {
		newsByIndexes = append(newsByIndexes, news[val])
	}

	return newsByIndexes
}

func sortByPriority(feeds []Feed) {
	for i := 1; i < len(feeds); i++ {
		if groupedNews, existGroup := feeds[i].Payload.(CompanyNews); existGroup {
			if singleNews, existSngle := feeds[i-1].Payload.(News); existSngle {
				if groupedNews.PublishTime == singleNews.PublushTime {
					feeds[i-1], feeds[i] = feeds[i], feeds[i-1]
				}
			}
		}

	}

}

func IsSliceEqual(a, b []string) bool {
	set := make(map[string]bool)

	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}
	for i := range a {
		set[a[i]] = true
	}

	for i := range a {
		if !set[b[i]] {
			return false
		}
	}

	return true
}

func groupNews(news []News) []Feed {
	var feeds []Feed
	alreadySeen := make(map[int64]bool)

	for i, val := range news {

		var newsIndexes []int
		newsIndexes = append(newsIndexes, i)

		if alreadySeen[val.ID] {
			continue
		}

		for nextNews := i + 1; nextNews < len(news); nextNews++ {
			if val.PublushTime.Day() != news[nextNews].PublushTime.Day() {
				break
			}

			if IsSliceEqual(val.Ticker, news[nextNews].Ticker) && !alreadySeen[news[nextNews].ID] {
				alreadySeen[news[nextNews].ID] = true
				newsIndexes = append(newsIndexes, nextNews)

			}

		}

		var feedSample Feed
		if len(newsIndexes) == 1 {
			feedSample = Feed{
				Type:    "news",
				Payload: news[newsIndexes[0]],
			}

		} else {
			tempPayload := getNewsByIndexes(newsIndexes, news)
			feedSample = Feed{
				Type: "company_news",
				Payload: CompanyNews{
					PayloadCompany: tempPayload,
					PublishTime:    news[newsIndexes[0]].PublushTime,
					Ticker:         news[newsIndexes[0]].Ticker,
				},
			}

		}

		feeds = append(feeds, feedSample)

	}

	return feeds

}

func writeToJSON(feeds []Feed, fileName string) error {
	sample, err := json.MarshalIndent(feeds, "", "    ")
	if err != nil {
		return fmt.Errorf("can't marshal some feeds: %s", err)
	}

	file, err := os.Create(fileName)
	if err != nil {
		return fmt.Errorf("can't create a file: %s", err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	defer writer.Flush()

	_, err = writer.Write(sample)
	if err != nil {
		return fmt.Errorf("can't write a result: %s", err)
	}
	return nil

}

func main() {
	path := flag.String("file", "newser.json", "filePath")
	flag.Parse()

	news, err := readNews(*path)
	if err != nil {
		log.Fatal("can't read JSON file : ", err)
	}

	newsSort(news)
	totalNews := groupNews(news)
	sortByPriority(totalNews)

	err = writeToJSON(totalNews, "out.json")
	if err != nil {
		log.Fatal("can't write a result:", err)
	}

}
