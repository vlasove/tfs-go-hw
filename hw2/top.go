package main

import "fmt"

func testEq(a, b []string) bool {
	set := make(map[string]bool)

	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}
	for i := range a {
		set[a[i]] = true
	}

	for i := range a {
		if !set[b[i]] {
			return false
		}
	}

	return true
}

func main() {
	fmt.Println(testEq([]string{"lol", "nekek"}, []string{"lol", "kek"}))
	fmt.Println([]string{} == nil, []string{} == nil)
	fmt.Println(([]string{} == nil) != ([]string{"kek"} == nil))

}
